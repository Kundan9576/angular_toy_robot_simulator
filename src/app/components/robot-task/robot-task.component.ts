import { Component, OnInit, Renderer2,  ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-robot-task',
  templateUrl: './robot-task.component.html',
  styleUrls: ['./robot-task.component.css']
})
export class RobotTaskComponent implements OnInit {
  
  xAxis: number = 2;
  yAxis: number = 2;
  direction: string ="south";
  directions = ["north", "east", "south", "west" ];
  dir_index: number = 0;
  output:string = "";
  
  tileSizeX: number = 5; // grid size in x direction
  tileSizeY: number = 5; // grid size in y-direction

  code = [];


 @ViewChild( 'tileset', {static: false} ) tileset: ElementRef;
  
  constructor( private renderer: Renderer2, private cdref: ChangeDetectorRef ) { }

  ngOnInit() {
    
  }

  ngAfterContentInit() {
    this.cdref.detectChanges();    
    this.place();
    this.clear();
    
     }

  render_pic( x, y , d ){
    // clearing the tiles
    this.tileset.nativeElement.innerHTML = "";

    for( var row = 0; row < this.tileSizeY; row ++){
      let tr = this.renderer.createElement('tr');
      for( var col = 0; col < this.tileSizeX; col ++){
        let td = this.renderer.createElement('td');
        if( ( ( row % 2 ) + col ) % 2 == 0 ) {
          td.className = "odd-bg";
        }
        else{
          td.className = "even-bg";
        }

        if( x == col && ( (this.tileSizeY - 1) - y ) == row) {
          let robot = this.renderer.createElement('img');
          robot.className = "robot robot-" + this.directions[d];
          robot.src = "../../../assets/images/robot.png";
          td.appendChild(robot);
        }
        // td.innerHTML = "td"; 
        tr.appendChild(td);         
      }
      this.renderer.appendChild(this.tileset.nativeElement, tr); 
    }
  }

  left(){
    this.dir_index = this.dir_index > 0 ? this.dir_index - 1 : 3;
    this.render_pic(this.xAxis, this.yAxis, this.dir_index);
    var code_enter = "LEFT";
    this.code.push(code_enter);
  }

  right(){
    this.dir_index = this.dir_index < 3 ? this.dir_index + 1 : 0;
    this.render_pic(this.xAxis, this.yAxis, this.dir_index);
    var code_enter = "RIGHT";
    this.code.push(code_enter);
  }

  report(){
    console.log(this.dir_index, this.direction)
  this.output = this.xAxis + ", " + this.yAxis + ", " + this.directions[this.dir_index].toUpperCase();
  }

  move(){
    switch ( this.dir_index ){
      case 0:
        this.yAxis = this.yAxis < (this.tileSizeY -1) ? this.yAxis + 1: 0;
      break;
      case 1:
        this.xAxis = this.xAxis < (this.tileSizeX - 1)? this.xAxis + 1: 0;
      break;
      case 2:
        this.yAxis = this.yAxis > 0? this.yAxis - 1: (this.tileSizeY -1);
      break;
      case 3:
        this.xAxis = this.xAxis > 0? this.xAxis - 1: (this.tileSizeX - 1);
      break;
    }
    this.render_pic(this.xAxis, this.yAxis, this.dir_index);
    var code_enter = "Move";
    this.code.push(code_enter);
  }

  place(){
    if( this.xAxis != undefined && this.yAxis != undefined && this.direction !=""){
      // console.log(this.xAxis, this.yAxis, this.direction);
      switch ( this.direction ){
        case "north": 
          this.dir_index = 0;
        break;
        case "east":
          this.dir_index = 1;
        break;
        case "south":
          this.dir_index = 2;
        break;
        case "west":
          this.dir_index = 3;
        break;
        default:
          this.dir_index = 0;
      }
      this.render_pic(this.xAxis, this.yAxis, this.dir_index);
      var code_enter = "Place " + this.xAxis + ", " + this.yAxis + ", " + this.direction;
      this.code.push( code_enter );
      
    }
    else{
      window.alert("Please enter valid input.");
    }
    
  }

  clear(){
    this.xAxis = 2;
    this.yAxis = 2;
    this.dir_index = 2;
    this.direction = "south";
    this.place();
    this.code = [];
    this.output = "";
  }

  dimension(){
    this.clear();
  }



}
